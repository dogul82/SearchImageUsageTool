﻿namespace ImageSearch
{

    partial class SearchImageUsageTool
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.targetFolderSearch = new System.Windows.Forms.Button();
            this.targetPathTextBox = new System.Windows.Forms.TextBox();
            this.SearchStart = new System.Windows.Forms.Button();
            this.messageListBox = new System.Windows.Forms.ListBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.searchPathTextBox = new System.Windows.Forms.TextBox();
            this.searchFolderSearch = new System.Windows.Forms.Button();
            this.TargetFolderText = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // targetFolderSearch
            // 
            this.targetFolderSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.targetFolderSearch.Location = new System.Drawing.Point(936, 12);
            this.targetFolderSearch.Name = "targetFolderSearch";
            this.targetFolderSearch.Size = new System.Drawing.Size(75, 23);
            this.targetFolderSearch.TabIndex = 0;
            this.targetFolderSearch.Text = "폴더찾기";
            this.targetFolderSearch.UseVisualStyleBackColor = true;
            this.targetFolderSearch.Click += new System.EventHandler(this.FolderSearchClick);
            // 
            // targetPathTextBox
            // 
            this.targetPathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.targetPathTextBox.Location = new System.Drawing.Point(102, 14);
            this.targetPathTextBox.Name = "targetPathTextBox";
            this.targetPathTextBox.Size = new System.Drawing.Size(828, 21);
            this.targetPathTextBox.TabIndex = 1;
            this.targetPathTextBox.TextChanged += new System.EventHandler(this.TargetPathTextBoxTextChanged);
            // 
            // SearchStart
            // 
            this.SearchStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchStart.Location = new System.Drawing.Point(842, 93);
            this.SearchStart.Name = "SearchStart";
            this.SearchStart.Size = new System.Drawing.Size(149, 23);
            this.SearchStart.TabIndex = 2;
            this.SearchStart.Text = "검색시작";
            this.SearchStart.UseVisualStyleBackColor = true;
            this.SearchStart.Click += new System.EventHandler(this.SearchStartClick);
            // 
            // messageListBox
            // 
            this.messageListBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.messageListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.messageListBox.FormattingEnabled = true;
            this.messageListBox.ItemHeight = 12;
            this.messageListBox.Location = new System.Drawing.Point(12, 122);
            this.messageListBox.Name = "messageListBox";
            this.messageListBox.Size = new System.Drawing.Size(1080, 328);
            this.messageListBox.TabIndex = 3;
            this.messageListBox.SelectedIndexChanged += new System.EventHandler(this.MessageListBoxSelectedIndexChanged);
            // 
            // searchPathTextBox
            // 
            this.searchPathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchPathTextBox.Location = new System.Drawing.Point(102, 50);
            this.searchPathTextBox.Name = "searchPathTextBox";
            this.searchPathTextBox.Size = new System.Drawing.Size(828, 21);
            this.searchPathTextBox.TabIndex = 4;
            this.searchPathTextBox.TextChanged += new System.EventHandler(this.SearchPathTextBoxTextChanged);
            // 
            // searchFolderSearch
            // 
            this.searchFolderSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.searchFolderSearch.Location = new System.Drawing.Point(936, 50);
            this.searchFolderSearch.Name = "searchFolderSearch";
            this.searchFolderSearch.Size = new System.Drawing.Size(75, 23);
            this.searchFolderSearch.TabIndex = 5;
            this.searchFolderSearch.Text = "폴더찾기";
            this.searchFolderSearch.UseVisualStyleBackColor = true;
            this.searchFolderSearch.Click += new System.EventHandler(this.SearchFolderSearchClick);
            // 
            // TargetFolderText
            // 
            this.TargetFolderText.AutoSize = true;
            this.TargetFolderText.Location = new System.Drawing.Point(22, 19);
            this.TargetFolderText.Name = "TargetFolderText";
            this.TargetFolderText.Size = new System.Drawing.Size(65, 12);
            this.TargetFolderText.TabIndex = 6;
            this.TargetFolderText.Text = "이미지폴더";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "검색지정폴더";
            // 
            // SearchImageUsageTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1117, 470);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TargetFolderText);
            this.Controls.Add(this.searchFolderSearch);
            this.Controls.Add(this.searchPathTextBox);
            this.Controls.Add(this.messageListBox);
            this.Controls.Add(this.SearchStart);
            this.Controls.Add(this.targetPathTextBox);
            this.Controls.Add(this.targetFolderSearch);
            this.Name = "SearchImageUsageTool";
            this.Text = "이미지 참조 검색 툴";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button targetFolderSearch;
        private System.Windows.Forms.TextBox targetPathTextBox;
        private System.Windows.Forms.Button SearchStart;
        private System.Windows.Forms.ListBox messageListBox;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox searchPathTextBox;
        private System.Windows.Forms.Button searchFolderSearch;
        private System.Windows.Forms.Label TargetFolderText;
        private System.Windows.Forms.Label label1;
    }
}

