﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace SearchImageUsageTool
{
    public class TargetData
    {
        public string name;
        public string fileName;
        public string guid;
        public string path;
    }

    public class SearchData
    {
        public string name;
        public List<string> guidList;
        public List<string> spriteNameList;
        public string path;
    }
    public class Util
    {
        public static DateTime Delay(int MS)
        {
            DateTime ThisMoment = DateTime.Now;
            TimeSpan duration = new TimeSpan(0, 0, 0, 0, MS);
            DateTime AfterWards = ThisMoment.Add(duration);

            while (AfterWards >= ThisMoment)
            {
                System.Windows.Forms.Application.DoEvents();
                ThisMoment = DateTime.Now;
            }

            return DateTime.Now;
        }
    }
    public class SearchImageUsageToolManager
    {
        private StreamWriter streamWriter;
        private List<TargetData> targetDataList = new List<TargetData>();
        private List<SearchData> searchDataList = new List<SearchData>();
        //  private string targetFolderPathText = @"..\JDrive\client\Assets\Graphic\UI\Old";
        //  private string searchFolderPathText = @"..\JDrive\client\Assets";
        public string targetFolderPathText { get; set; }
        public string searchFolderPathText { get; set; }
        public System.Windows.Forms.ListBox messageListBox { set; get; }
        private List<string> messageBoxlist = new List<string>();

        public SearchImageUsageToolManager()
        {
        }

        public void StartSearch()
        {
              targetDataList.Clear();
              searchDataList.Clear();
              if (TargetDataSetting() == false) return;
              if (SearchDataSetting() == false) return;
              Searching();
           
              MessageLog("겁색완료");
        }

        private string GetGuid(string fullName)
        {
            string text = System.IO.File.ReadAllText(fullName);
            bool isGuid = text.Contains("guid:");
            if (isGuid == false) return string.Empty;

            string[] parts = text.Split(' ', '\n', ',');
            for (int index = 0; index < parts.Length; index++)
            {
                if (parts[index].Contains("guid:"))
                {
                    index++;
                    return parts[index];
                }
            }
            return string.Empty;
        }

        private List<string> GetGuidList(string fullName)
        {
            List<string> guidList = new List<string>();

            string text = System.IO.File.ReadAllText(fullName);
            bool isGuid = text.Contains("guid:");
            if (isGuid == false) return guidList;

            string[] parts = text.Split(' ', '\n', ',');
            for (int index = 0; index < parts.Length; index++)
            {
                if (parts[index].Contains("guid:"))
                {
                    index++;
                    guidList.Add(parts[index]);
                }
            }
            return guidList;
        }

        private List<string> GetSpriteNameList(string fullName)
        {
            List<string> spriteName = new List<string>();

            string fileReadText = System.IO.File.ReadAllText(fullName);
            bool isGuid = fileReadText.Contains("spriteName:");
            if (isGuid == false) return spriteName;

            string[] parts = fileReadText.Split(' ', '\n', ',');
            for (int index = 0; index < parts.Length; index++)
            {
                if (parts[index].Contains("spriteName:"))
                {
                    index++;
                    spriteName.Add(parts[index]);
                }
            }
            return spriteName;
        }

        private bool TargetDataSetting()
        {
            DirectoryInfo dir = new DirectoryInfo(targetFolderPathText);
            if (dir.Exists == false)
            {
                MessageLog("이미지폴더 경로가 잘못됬습니다.");
                return false;
            }
            MessageLog("이미지 GUID 추출");
            string[] searchType = { "*.png.meta", "*.jpg.meta", "*.tga.meta" };
            foreach (var type in searchType)
            {
                int Count = 0;

                foreach (var i in dir.GetFiles(type, SearchOption.AllDirectories))
                {
                    string guid = GetGuid(i.FullName);
                    TargetData data = new TargetData();
                    data.path = i.FullName;
                    data.fileName = i.Name;
                    string[] names = Path.GetFileName(i.Name).Split('.');
                    data.name = names[0];
                    data.guid = guid;
                    targetDataList.Add(data);
                    Count++;
                }

                MessageLog(string.Format("{0}Count :{1} ", type, Count));
            }
            MessageLog(string.Format(" 이미지 GUID 추출 완료  {0}개", targetDataList.Count));
            return true;
        }

        private bool SearchDataSetting()
        {
            DirectoryInfo dir = new DirectoryInfo(searchFolderPathText);
            if (dir.Exists == false)
            {
                MessageLog("검색하려는 폴더 경로가 잘못됬습니다.");
                return false;
            }
            MessageLog("검색 파일 사용 GUID 추출");

            string[] searchTypeArray = { "*.unity", "*.prefab", "*.mat" };

            foreach (var type in searchTypeArray)
            {
                MessageLog(string.Format("진행 타입 {0}", type));
                int count = 0;
                foreach (var i in dir.GetFiles(type, SearchOption.AllDirectories))
                {
                    List<string> guidList = GetGuidList(i.FullName);
                    List<string> spriteNameList = GetSpriteNameList(i.FullName);
                    SearchData data = new SearchData();
                    data.path = i.FullName;
                    data.name = i.Name;
                    data.guidList = guidList;
                    data.spriteNameList = spriteNameList;
                    searchDataList.Add(data);
                    count++;
                }
                MessageLog(string.Format("name [{0}] \\t cound: [{1}]", type, count));

            }
            MessageLog(string.Format("Unity, Prefab GUID 추출 파일 수 {0}", searchDataList.Count));
            return true;
        }


        private void Searching()
        {
            string[] fileNames = Path.GetFileName(targetFolderPathText).Split('.');
            MessageLog(fileNames[0]);
            string filePath = string.Format(".\\{0}.txt", fileNames[0]);
            streamWriter = new StreamWriter(filePath, false);
            StreamWriterAdd(string.Format("TargetFolder {0}", targetFolderPathText));

            foreach (var targetData in targetDataList)
            {
                StreamWriterAdd("============================================================================");
                StreamWriterAdd(string.Format("아이콘이름: [{0}]", targetData.fileName));
                StreamWriterAdd(string.Format("아이콘경로: [{0}]", targetData.path));

                bool isEqual = false;
                int count = 0;
                StreamWriterAdd("참조파일:");

                foreach (var searchData in searchDataList)
                {
                    foreach (var guid in searchData.guidList)
                    {
                        if (targetData.guid.Equals(guid))
                        {
                            string equalsMessage = string.Format("\t\t{0}", searchData.path);
                            StreamWriterAdd(equalsMessage);
                            isEqual = true;
                            count++;
                        }
                    }

                }

                StreamWriterAdd("SpriteName 로드:");
                foreach (var searchData in searchDataList)
                {
                    foreach (var sprtieName in searchData.spriteNameList)
                    {
                        if (targetData.name.Equals(sprtieName))
                        {
                            string equalsMessage = string.Format("\t\t{0}", searchData.path);
                            StreamWriterAdd(equalsMessage);
                            isEqual = true;
                            count++;
                        }
                    }
                }

                if (isEqual == false)
                {
                    string message = string.Format("사용하는 파일이 없습니다.");
                    StreamWriterAdd(string.Format("아이콘경로: [{0}]", targetData.path));
                }
                else
                {
                    string message = string.Format("사용하는 참조 위치가 [{0}]곳 있습니다.", count);
                    StreamWriterAdd(message);
                }

                MessageLog(string.Format("{0} 검색완료", targetData.name));
                StreamWriterAdd("\n");

                Util.Delay(1);
            }
            streamWriter.Close();

        }


        private void LogClear()
        {
            messageListBox.Items.Clear();
        }

        private void MessageLog(string msg)
        {
            messageListBox.Items.Add(msg);
            Util.Delay(1);
        }
        private void StreamWriterAdd(string msg)
        {
            streamWriter.WriteLine(msg);

            MessageLog(msg);
            messageListBox.SelectedIndex = messageListBox.Items.Count - 1;
        }


    }
}
