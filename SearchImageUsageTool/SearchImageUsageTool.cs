﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using SearchImageUsageTool;


namespace ImageSearch
{
// Target
    public partial class SearchImageUsageTool : Form
    {
        SearchImageUsageToolManager toolManager = null;
        public SearchImageUsageTool()
        {
            InitializeComponent();
            toolManager = new SearchImageUsageToolManager();
            targetPathTextBox.Text = string.Empty;
            searchPathTextBox.Text = string.Empty;
            toolManager.messageListBox = messageListBox;
        }
        private void FolderSearchClick(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.SelectedPath = toolManager.searchFolderPathText;
            dialog.ShowDialog();
            string select_path = dialog.SelectedPath;
            targetPathTextBox.Text = select_path;
            toolManager.targetFolderPathText = targetPathTextBox.Text;
        }

        private void SearchFolderSearchClick(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.SelectedPath = toolManager.searchFolderPathText;
            dialog.ShowDialog();
            string selectPath = dialog.SelectedPath;
            searchPathTextBox.Text = selectPath;
            toolManager.searchFolderPathText = targetPathTextBox.Text;
        }

        private void SearchStartClick(object sender, EventArgs e)
        {
            toolManager.StartSearch();
        }
        private void TargetPathTextBoxTextChanged(object sender, EventArgs e)
        {
            toolManager.targetFolderPathText = targetPathTextBox.Text;
        }

        private void SearchPathTextBoxTextChanged(object sender, EventArgs e)
        {
            toolManager.searchFolderPathText = searchPathTextBox.Text;
        }

        private void MessageListBoxSelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
